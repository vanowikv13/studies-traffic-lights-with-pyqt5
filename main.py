#!/usr/bin/env python
from trafficLights import TrafficLights
from PyQt5.QtWidgets import QApplication
from sys import exit, argv

if __name__ == '__main__':
    App = QApplication([])
    trafficLights = TrafficLights()
    exit(App.exec())