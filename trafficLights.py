from PyQt5.QtGui import QPainter, QPen, QBrush, QFont
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt, QThread, QObject, pyqtSignal


class TrafficLightsWorker(QObject):
    finished = pyqtSignal()

    # static configs
    greenInterval = 2
    redInterval = 2
    yellowInterval = 1

    greenColor = Qt.gray
    redColor = Qt.gray
    yellowColor = Qt.gray

    stopped = True

    def __init__(self, updateWindow, parent=None):
        QObject.__init__(self, parent=None)
        self.updateWindow = updateWindow
        self.queue = [self.__changeToRed, self.__changeToRedYellow, self.__changeToGreen, self.__changeToYellow]

    def __changeToRed(self):
        TrafficLightsWorker.redColor = Qt.red
        self.updateWindow()
        QThread.sleep(TrafficLightsWorker.redInterval)

    def __changeToRedYellow(self):
        TrafficLightsWorker.yellowColor = Qt.yellow
        TrafficLightsWorker.redColor = Qt.red
        self.updateWindow()
        QThread.sleep(TrafficLightsWorker.yellowInterval)

    def __changeToGreen(self):
        TrafficLightsWorker.yellowColor = Qt.gray
        TrafficLightsWorker.redColor = Qt.gray
        TrafficLightsWorker.greenColor = Qt.green
        self.updateWindow()
        QThread.sleep(TrafficLightsWorker.greenInterval)

    def __changeToYellow(self):
        TrafficLightsWorker.greenColor = Qt.gray
        TrafficLightsWorker.yellowColor = Qt.yellow
        self.updateWindow()
        QThread.sleep(TrafficLightsWorker.yellowInterval)
        TrafficLightsWorker.yellowColor = Qt.gray

    def paintTrafficLights(self):
        while True:
            while not TrafficLightsWorker.stopped:
                trafficAction = self.queue.pop(0)
                trafficAction()
                self.queue.append(trafficAction)
            TrafficLightsWorker.greenColor = Qt.gray
            TrafficLightsWorker.redColor = Qt.gray
            TrafficLightsWorker.yellowColor = Qt.gray
            self.updateWindow()

    # change from stop to start and from start to stop
    @staticmethod
    def changeStatus():
        TrafficLightsWorker.stopped = not TrafficLightsWorker.stopped

    @staticmethod
    def stop():
        TrafficLightsWorker.stopped = True


class TrafficLights(QWidget):
    signal = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.__initUI()

    def __initUI(self):
        self.setWindowTitle('Traffic Lights')
        self.setGeometry(600, 500, 500, 400)

        # Labels with input fields
        redIntervalLabel = QLabel(self)
        self.__setupQtElement(redIntervalLabel, 190, 30, 'Red light interval s:', 14, 300)
        self.redIntervalInputField = QLineEdit(self)
        self.__setupQtElement(self.redIntervalInputField, 400, 30, '2', 14, 70)

        yellowIntervalLabel = QLabel(self)
        self.__setupQtElement(yellowIntervalLabel, 190, 80, 'Yellow light interval s:', 14, 300)
        self.yellowIntervalInputField = QLineEdit(self)
        self.__setupQtElement(self.yellowIntervalInputField, 400, 80, '1', 14, 70)

        greenIntervalLabel = QLabel(self)
        self.__setupQtElement(greenIntervalLabel, 190, 130, 'Green light interval s:', 14, 300)
        self.greenIntervalInputField = QLineEdit(self)
        self.__setupQtElement(self.greenIntervalInputField, 400, 130, '2', 14, 70)

        # Button
        self.button = QPushButton(self)
        self.__setupQtElement(self.button, 274, 350, 'Start', 18, 100)

        # Thread
        self.thread = QThread()
        self.worker = TrafficLightsWorker(self.update)
        self.signal.connect(self.worker.changeStatus)
        self.worker.moveToThread(self.thread)
        self.worker.finished.connect(self.thread.quit)
        self.worker.finished.connect(self.worker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)

        self.thread.started.connect(self.worker.paintTrafficLights)
        self.thread.finished.connect(self.worker.changeStatus)
        self.thread.start()

        self.button.clicked.connect(self.onClick)
        self.__start()

    def __setupQtElement(self, element, xPos, yPos, text, fontSize, fixedWidth, alignment=None, style=None):
        element.setFont(QFont("Arial", fontSize, QFont.Bold))
        element.setText(text)
        element.move(xPos, yPos)
        element.setFixedWidth(fixedWidth)
        if alignment != None:
            element.setAlignment(alignment)
        if style != None:
            element.setStyleSheet(style)
        return element

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setPen(QPen(Qt.black, 8, Qt.SolidLine))
        painter.setBrush(QBrush(Qt.black, Qt.SolidPattern))
        painter.drawRect(30, 30, 120, 340)
        painter.setBrush(QBrush(TrafficLightsWorker.redColor, Qt.SolidPattern))
        painter.drawEllipse(40, 40, 100, 100)
        painter.setBrush(QBrush(TrafficLightsWorker.yellowColor, Qt.SolidPattern))
        painter.drawEllipse(40, 150, 100, 100)
        painter.setBrush(QBrush(TrafficLightsWorker.greenColor, Qt.SolidPattern))
        painter.drawEllipse(40, 260, 100, 100)

    def onClick(self):
        if not self.worker.stopped:
            TrafficLightsWorker.greenInterval = int(self.greenIntervalInputField.text())
            TrafficLightsWorker.redInterval = int(self.redIntervalInputField.text())
            TrafficLightsWorker.yellowInterval = int(self.yellowIntervalInputField.text())
            self.button.setText('Start')
            TrafficLightsWorker.changeStatus()
        else:
            self.button.setText('Stop')
            TrafficLightsWorker.changeStatus()

    def __start(self):
        self.show()
